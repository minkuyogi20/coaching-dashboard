import hashing
import models,schemas
from sqlalchemy.orm import Session
from fastapi import HTTPException,status
from . import user
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def get_students(db:Session):
    try:
        students = db.query(models.Student).all()
        student_list = [{
            'id': student.id,
            'name': student.name,
            'email':student.email,
            'address':student.address,
            'phone_number':student.phone_number,
            'branch_address': student.branch.address,
            'branch_location': student.branch.location,
            'course_name': student.course.course_name,
            'batch_timings':student.batch.timing
        } for student in students]
        return student_list
    except Exception as e:
        return {'error': str(e)}

def get_student_with_id(student_id,db:Session):
    try:
        students = db.query(models.Student).all()
        student_list = [{
            'id': student.id,
            'name': student.name,
            'email': student.email,
            'address': student.address,
            'phone_number': student.phone_number,
            'branch_address': student.branch.address,
            'branch_location': student.branch.location,
            'course_name': student.course.course_name,
            'batch_timings': student.batch.timing
        } for student in students if student.id == student_id]
        return student_list
    except Exception as e:
        return {'error': str(e)}

def create(request:schemas.CreateStudent,db:Session):
    existing_student = db.query(models.Student).filter_by(email=request.email).first()
    if existing_student:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Email already exists")
    new_student = models.Student(name=request.name,email=request.email,address=request.address,phone_number=request.phone_number,
                                 branch_id=request.branch_id,course_id=request.course_id,batch_id=request.batch_id )
    db.add(new_student)
    db.commit()
    db.refresh(new_student)

    new_user = models.User(email=request.email,role="Student")
    db.add(new_user)
    db.commit()
    db.refresh(new_user)

    user_id = new_user.id
    new_student.user_id = user_id
    db.add(new_student)
    db.commit()
    db.refresh(new_student)

    email_subject = "Change Your Password"
    email_message = "Dear student, please click on the following link to change your password: [Link to password change page]"
    send_email(request.email, email_subject, email_message)
    return "New User Added"

def update(student_id,request:schemas.ShowStudent,db:Session):
    student = db.query(models.Student).filter(models.Student.id==student_id)
    if not student.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The student with student id {student_id} is not found")
    student.update(request.dict())
    db.commit()
    return "Updated"

def delete(student_id,db:Session):
    student = db.query(models.Student).filter(models.Student.id == student_id)
    if not student.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The student with student id {student_id} is not found")
    student.delete(synchronize_session=False)
    db.commit()
    return "Deleted"

def send_email(to_email, subject, message):
    # Your email and password for the SMTP server
    smtp_server = 'smtp.gmail.com'  # Replace with your SMTP server address
    smtp_port = 587  # Replace with your SMTP server port
    smtp_username = 'minkuyogi20'
    smtp_password = 'oohiusvthyfjzkuh'

    # Create a secure SMTP connection
    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(smtp_username, smtp_password)

    # Create the email message
    msg = MIMEMultipart()
    msg['From'] = 'minkuyogi20@gmail.com'  # Replace with your email address
    msg['To'] = to_email
    msg['Subject'] = subject

    # Attach the message to the email
    msg.attach(MIMEText(message, 'plain'))

    # Send the email
    server.sendmail(smtp_username, to_email, msg.as_string())

    # Close the SMTP server connection
    server.quit()

def update_password_func(email,request,db:Session):
    user = db.query(models.User).filter(models.User.email == email).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="User is not valid")
    if request.password != request.confirm_password:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="Password doesn't match")
    else:
        user.password = request.password
        hashed_password = hashing.Hash.bcrypt(user.password)
        user.password = hashed_password
        db.add(user)
        db.commit()
        db.refresh(user)
        return "Updated"


