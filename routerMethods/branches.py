import models,schemas
from sqlalchemy.orm import Session
from fastapi import HTTPException,status

def get_all(db:Session):
    branches = db.query(models.Branch).all()
    return branches

# def get_branch(id,db:Session):
#     branches = db.query(models.Branch).all()
#     for branch in branches:
#         if branch.id == id:
#             return branch
#     raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The id {id} is not found in the branches")

def get_branch_new(id,db:Session):
    courses_list = db.query(models.Course).all()
    courses = [{'id': course.id,'course_name': course.course_name} for course in courses_list if course.branch_id == id]
    # branch_list = [{'id': branch.id, 'courses':courses}for branch in branches if branch.id == id]
    return courses

# def get_branch_course(id,db:Session):
#     branches=db.query(models.Branch).all()
#     branch_list = [{'id':branch.id,'address':branch.address,'location':branch.location,
#                     'course_name':[course.course_name for course in branch.course],'course_id':[course.id for course in branch.course]}for branch in branches if branch.id == id]
#     return branch_list

# def get_branch_course(id,db:Session):
#     branches = db.query(models.Branch).all()
#     courses_list = db.query(models.Course).all()
#     # batches_list = db.query(models.Batch).all()
#     # batches = [{'timing': batch.timing, 'id': batch.id} for batch in batches_list if batch.course_id == courses_list.id]
#     courses = [{'course_name':course.course_name,'id':course.id}for course in courses_list if course.branch_id == id]
#     # batches = [{'timing':batch.timing,'id':batch.id}for batch in batches_list if batch.course_id == courses_list.id]
#     branch_list = [{'id':branch.id,'address':branch.address,'location':branch.location,
#                     'courses':courses}for branch in branches if branch.id == id]
#     return branch_list

def create(request:schemas.Branch,db:Session):
    new_branch = models.Branch(address=request.address,location=request.location)
    db.add(new_branch)
    db.commit()
    db.refresh(new_branch)
    return "New Branch Added"

def update(branch_id,request:schemas.Branch,db:Session):
    branch=db.query(models.Branch).filter(models.Branch.id==branch_id)
    if not branch.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No branch is found")
    branch.update(request.dict())
    db.commit()
    return "Updated"

def delete(branch_id,db:Session):
    branch=db.query(models.Branch).filter(models.Branch.id==branch_id)
    if not branch.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="The branch is not found")
    branch.delete(synchronize_session=False)
    db.commit()
    return "Deleted"