import models,schemas
from sqlalchemy.orm import Session
from fastapi import HTTPException,status

def get_all(db:Session):
    try:
        batches = db.query(models.Batch).all()
        batch_list = [{'id': batch.id, 'course_name':batch.course.course_name,'timing':batch.timing} for batch in batches]
        return batch_list
    except Exception as e:
        return {'error': str(e)}

def get_all_with_id(batch_id,db:Session):
    try:
        batches = db.query(models.Batch).all()
        batch_list = [{'id': batch.id, 'course_name':batch.course.course_name,'timing':batch.timing} for batch in batches if batch.id == batch_id]
        return batch_list
    except Exception as e:
        return {'error': str(e)}

def create(request:schemas.ShowBatch,db:Session):
    new_batch = models.Batch(timing=request.timing,course_id=request.course_id)
    db.add(new_batch)
    db.commit()
    db.refresh(new_batch)
    return "New Batch Added"

# def create(request:schemas.Batch_new,db:Session):
#     new_batch=db.query(models.Course).all()
#     course_id=-2
#     for batch in new_batch:
#         if batch.course_name == request.course_name:
#             course_id = batch.id
#     if course_id==-2:
#         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="the address and location is invalid")
#     new_batch=models.Batch(timing=request.timing,course_id=course_id)
#     db.add((new_batch))
#     db.commit()
#     db.refresh(new_batch)
#     return "New Batch Added"

def update(batch_id,request:schemas.ShowBatch,db:Session):
    batch = db.query(models.Batch).filter(models.Batch.id==batch_id)
    if not batch.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The batch with id {batch_id} is not found")
    batch.update(request.dict())
    db.commit()
    return "Updated"

def delete(batch_id,db:Session):
    batch = db.query(models.Batch).filter(models.Batch.id==batch_id)
    if not batch.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The batch with id {batch_id} is not found")
    batch.delete(synchronize_session=False)
    db.commit()
    return "Deleted"