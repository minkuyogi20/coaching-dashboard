from datetime import timedelta
from fastapi import HTTPException,status
from sqlalchemy.orm import Session
import models,token_jwt
import schemas
from hashing import Hash

def login(request,db:Session):
    user=db.query(models.User).filter(request.username==models.User.email).first()
    user_detail = db.query(models.Student).filter(request.username==models.Student.email).first()
    user_detail_1 = db.query(models.Teacher).filter(request.username == models.Teacher.email).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="The user is not valid")
    if not Hash.verify(user.password,request.password):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="The password is not valid")
    # return user
    # access_token_expires = timedelta(minutes=token_jwt.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = token_jwt.create_access_token(
        data={"sub": user.email,"role":user.role}
        #, expires_delta=access_token_expires
    )
    refresh_token = token_jwt.create_refresh_token(
        data={"sub": user.email}
    )
    if user.role == "Student" :
        return {"access_token": access_token,"refresh_token":refresh_token,"token_type": "bearer",
                "name":user_detail.name,
                "address":user_detail.address,
                "email":user_detail.email,
                "role":user_detail.user.role}
    elif user.role == "Teacher" :
        return {"access_token": access_token, "refresh_token": refresh_token, "token_type": "bearer",
                "name": user_detail_1.name,
                "address": user_detail_1.address,
                "email": user_detail_1.email,
                "role":user_detail_1.user.role}
    else:
        return {"access_token": access_token, "refresh_token": refresh_token, "token_type": "bearer"}

def refresh_token(refresh_token:str):
    new_access_token = token_jwt.refresh_access_token(refresh_token)
    return {"access_token": new_access_token}
