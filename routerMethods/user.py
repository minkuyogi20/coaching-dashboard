import models,schemas
from sqlalchemy.orm import Session
from fastapi import HTTPException,status
import hashing

def get_all(db:Session):
    users = db.query(models.User).all()
    return users

def get_with_id(id,db:Session):
    user = db.query(models.User).filter(models.User.id==id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The user with id {id} is not exist")
    return user

def create(request:schemas.User,db:Session):
    new_user = models.User(email=request.email,password=hashing.Hash.bcrypt(request.password),role=request.role)
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return "New User Added"


def update(user_id,request:schemas.User,db:Session):
    user=db.query(models.User).filter(models.User.id==user_id)
    if not user.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No user is found")
    if request.password:
        hashed_password = hashing.Hash.bcrypt(request.password)
        request.password = hashed_password
    user.update(request.dict())
    db.commit()
    return "Updated"

def update_patch(user_id, request: schemas.User, db: Session):
    user = db.query(models.User).filter(models.User.id == user_id)

    if not user.first():
        raise HTTPException(status_code=404, detail="No user is found")

    updated_data = request.dict(exclude_unset=True)  # Exclude fields with None values

    if "password" in updated_data:
        hashed_password = hashing.Hash.bcrypt(updated_data["password"])
        updated_data["password"] = hashed_password

    user.update(updated_data)
    db.commit()
    return "Updated"

def delete(user_id,db:Session):
    user = db.query(models.User).filter(models.User.id==user_id)
    if not user.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="The branch is not found")
    user.delete(synchronize_session=False)
    db.commit()
    return "Deleted"