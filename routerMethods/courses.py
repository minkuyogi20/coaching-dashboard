import models,schemas
from sqlalchemy.orm import Session
from fastapi import HTTPException,status

def get_course_address(db:Session):
    try:
        courses = db.query(models.Course).all()
        course_list = [{'id': course.id, 'course_name': course.course_name} for course in courses]
        return course_list
    except Exception as e:
        return {'error': str(e)}

# def get_course_with_id(course_id,db:Session):
#     try:
#         courses = db.query(models.Course).all()
#         course_list=[{'id': course.id, 'course_name': course.course_name, 'branch_location': course.branch.location , 'branch_address': course.branch.address, 'branch_timing':[batch.timing for batch in course.batch]} for course in courses if course.id==course_id]
#         return course_list
#     except Exception as e:
#         return {'error': str(e)}

def get_course_with_id_new(course_id,db:Session):
    batch_list = db.query(models.Batch).all()
    courses = [{'id': batch.id, 'timing': batch.timing} for batch in batch_list if
               batch.course_id == course_id]
    # branch_list = [{'id': branch.id, 'courses':courses}for branch in branches if branch.id == id]
    return courses

def create(request:schemas.ShowCourse,db:Session):
    new_course = models.Course(course_name=request.course_name,branch_id=request.branch_id)
    branch = db.query(models.Branch).filter(request.branch_id==models.Branch.id).first()
    if not branch:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The branch is incorrect")
    db.add(new_course)
    db.commit()
    db.refresh(new_course)
    return "New Course Added"

# def create(request:schemas.Course_new,db:Session):
#     new_course=db.query(models.Branch).all()
#     branch_id=-2
#     for course in new_course:
#         if course.address == request.branch_address and course.location == request.branch_location:
#             branch_id = course.id
#     if branch_id==-2:
#         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="the address and location is invalid")
#     new_course=models.Course(course_name=request.course_name,branch_id=branch_id)
#     db.add((new_course))
#     db.commit()
#     db.refresh(new_course)
#     return "New Course Added"

def update(course_id,request:schemas.ShowCourse,db:Session):
    course = db.query(models.Course).filter(models.Course.id==course_id)
    if not course.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The course with course id {course_id} is not found")
    branch = db.query(models.Branch).filter(request.branch_id == models.Branch.id).first()
    if not branch:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"The branch is incorrect")
    course.update(request.dict())
    db.commit()
    return "Course Updated"

def update_patch(course_id,request:schemas.ShowCourse,db:Session):
    course = db.query(models.Course).filter(models.Course.id==course_id)
    if not course.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The course with course id {course_id} is not found")
    branch = db.query(models.Branch).filter(request.branch_id == models.Branch.id).first()
    if not  branch:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"The branch is incorrect")
    course.update(request.dict())
    db.commit()
    return "Course Updated"

def delete(course_id,db:Session):
    course = db.query(models.Course).filter(models.Course.id==course_id)
    if not course.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The course with course id {course_id} is not found")
    course.delete(synchronize_session=False)
    db.commit()
    return "Course Deleted"