import hashing
import models,schemas
from sqlalchemy.orm import Session
from fastapi import HTTPException,status

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def get_teachers(db:Session):
    try:
        teachers = db.query(models.Teacher).all()
        teacher_list = [{
            'id': teacher.id,
            'name': teacher.name,
            'email': teacher.email,
            'address': teacher.address,
            'phone_number': teacher.phone_number,
            'branch_address': teacher.branch.address,
            'branch_location': teacher.branch.location,
            'course_name': teacher.course.course_name,
            'batch_timings': teacher.batch.timing
        } for teacher in teachers]
        return teacher_list
    except Exception as e:
        return {'error': str(e)}

def get_teacher_with_id(teacher_id,db:Session):
    try:
        teachers = db.query(models.Teacher).all()
        teacher_list = [{
            'id': teacher.id,
            'name': teacher.name,
            'email':teacher.email,
            'address':teacher.address,
            'phone_number':teacher.phone_number,
            'branch_address': teacher.branch.address,
            'branch_location': teacher.branch.location,
            'course_name': teacher.course.course_name,
            'batch_timings':teacher.batch.timing
        } for teacher in teachers if teacher.id == teacher_id]
        return teacher_list
    except Exception as e:
        return {'error': str(e)}

def create(request:schemas.CreateStudent,db:Session):
    existing_teacher=db.query(models.Teacher).filter_by(email=request.email).first()
    if existing_teacher:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="Email already exists")
    new_teacher = models.Teacher(name=request.name,email=request.email,address=request.address,phone_number=request.phone_number,
                                 branch_id=request.branch_id,course_id=request.course_id,batch_id=request.batch_id)

    db.add(new_teacher)
    db.commit()
    db.refresh(new_teacher)

    new_user = models.User(email=request.email, role="Teacher")
    db.add(new_user)
    db.commit()
    db.refresh(new_user)

    user_id = new_user.id
    new_teacher.user_id = user_id
    db.add(new_teacher)
    db.commit()
    db.refresh(new_teacher)

    email_subject = "Change Your Password"
    email_message = "Dear teacher, please click on the following link to change your password: [Link to password change page]"
    send_email(request.email, email_subject, email_message)
    return "New User Added"

# def create(request:schemas.Teacher_new,db:Session):
#     new_teacher_branch = db.query(models.Branch).all()
#     new_teacher_course = db.query(models.Course).all()
#     new_teacher_batch = db.query(models.Batch).all()
#     course_id=0
#     branch_id=0
#     batch_id=0
#     for branch in new_teacher_branch:
#         if branch.address == request.branch_address and branch.location == request.branch_location:
#             branch_id = branch.id
#     for course in new_teacher_course:
#         if course.course_name == request.course_name:
#             course_id = course.id
#     for batch in new_teacher_batch:
#         if batch.timing == request.batch_timings:
#             batch_id = batch.id
#     if (course_id==0):
#         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="You are adding invalid course")
#     if (branch_id==0):
#         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="You are adding invalid branch")
#     if (batch_id==0):
#         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="You are adding invalid timing")
#     new_teacher=models.Teacher(name=request.name,email=request.email,address=request.address,phone_number=request.phone_number,
#                                 branch_id=branch_id,course_id=course_id,batch_id=batch_id)
#     db.add((new_teacher))
#     db.commit()
#     db.refresh(new_teacher)
#     return "New Teacher Added"

def update(teacher_id,request:schemas.ShowTeacher,db:Session):
    teacher = db.query(models.Teacher).filter(models.Teacher.id==teacher_id)
    if not teacher.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The teacher with teacher id {teacher_id} is not found")
    teacher.update(request.dict())
    db.commit()
    return "Updated"

def delete(teacher_id,db:Session):
    teacher = db.query(models.Teacher).filter(models.Teacher.id == teacher_id)
    if not teacher.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"The teacher with teacher id {teacher_id} is not found")
    teacher.delete(synchronize_session=False)
    db.commit()
    return "Deleted"

def send_email(to_email, subject, message):
    # Your email and password for the SMTP server
    smtp_server = 'smtp.gmail.com'  # Replace with your SMTP server address
    smtp_port = 587  # Replace with your SMTP server port
    smtp_username = 'minkuyogi20'
    smtp_password = 'oohiusvthyfjzkuh'

    # Create a secure SMTP connection
    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(smtp_username, smtp_password)

    # Create the email message
    msg = MIMEMultipart()
    msg['From'] = 'minkuyogi20@gmail.com'  # Replace with your email address
    msg['To'] = to_email
    msg['Subject'] = subject

    # Attach the message to the email
    msg.attach(MIMEText(message, 'plain'))

    # Send the email
    server.sendmail(smtp_username, to_email, msg.as_string())

    # Close the SMTP server connection
    server.quit()

def update_password_func(email,request,db:Session):
    user = db.query(models.User).filter(models.User.email == email).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="User is not valid")
    if request.password != request.confirm_password:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="Password doesn't match")
    else:
        user.password = request.password
        hashed_password = hashing.Hash.bcrypt(user.password)
        user.password = hashed_password
        db.add(user)
        db.commit()
        db.refresh(user)
        return "Updated"

