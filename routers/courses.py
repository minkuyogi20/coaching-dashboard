from fastapi import APIRouter,Depends,status
from typing import List
from sqlalchemy.orm import Session
import schemas,models,database,oauth2
from routerMethods import courses

router=APIRouter(
    prefix="/course",
    tags=["Courses"]
)

@router.get("/get",response_model=List[schemas.Branch_course])
async def get_all_courses(db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return courses.get_course_address(db)

# @router.get("/{course_id}",response_model=List[schemas.Course])
# async  def get_course_with_id(course_id:int,db:Session=Depends(database.get_db)):
#     return courses.get_course_with_id(course_id,db)

@router.get("/get/{course_id}",response_model=List[schemas.Course])
async  def get_course_with_id(course_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return courses.get_course_with_id_new(course_id,db)

# @router.post("/",status_code=status.HTTP_201_CREATED)
# async def create_course(request:schemas.ShowCourse,db:Session=Depends(database.get_db)):
#     return courses.create(request,db)

@router.post("/create",status_code=status.HTTP_201_CREATED)
async def create_course(request:schemas.ShowCourse,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return courses.create(request,db)

@router.put("/update/{course_id}")
async def update_course(course_id:int,request:schemas.ShowCourse,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return courses.update(course_id,request,db)

@router.patch("/update/{course_id}/patch")
async def update_course(course_id:int,request:schemas.ShowCourse,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return courses.update_patch(course_id,request,db)

@router.delete("/delete/{course_id}")
async def delete_course(course_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return courses.delete(course_id,db)