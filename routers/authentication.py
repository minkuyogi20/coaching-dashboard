from fastapi import APIRouter,status,Depends

import schemas,database
from sqlalchemy.orm import Session
from routerMethods import loginMethod
from fastapi.security import OAuth2PasswordRequestForm


router = APIRouter(
    prefix = "/login",
    tags = ["Login"]
)

@router.post("/",status_code=status.HTTP_201_CREATED)
async def create_login(request:OAuth2PasswordRequestForm=Depends(),db:Session=Depends(database.get_db)):
    return loginMethod.login(request,db)

# @router.post("/refresh",status_code=status.HTTP_201_CREATED)
# async def refresh(request:schemas.Refresh):
#     return loginMethod.refresh_token(request)

@router.post("/refresh", status_code=status.HTTP_201_CREATED)
async def refresh(request: schemas.Refresh):
    return loginMethod.refresh_token(request.refresh_token)







