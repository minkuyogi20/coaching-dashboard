from fastapi import APIRouter,Depends,status
from typing import List
from sqlalchemy.orm import Session
import schemas,models,database,oauth2
from routerMethods import teachers

router=APIRouter(
    prefix="/teachers",
    tags=["Teachers"]
)

@router.get("/get",response_model=List[schemas.Teacher])
async def get_all_teachers(db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return teachers.get_teachers(db)

@router.get("/get/{teacher_id}",response_model=List[schemas.Teacher])
async  def get_teacher_with_id(teacher_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return teachers.get_teacher_with_id(teacher_id,db)

# @router.post("/",status_code=status.HTTP_201_CREATED)
# async def create_teacher(request:schemas.ShowTeacher,db:Session=Depends(database.get_db)):
#     return teachers.create(request,db)

@router.post("/create",status_code=status.HTTP_201_CREATED)
async def create_teacher(request:schemas.CreateStudent,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return teachers.create(request,db)

@router.put("/update/{teacher_id}")
async def update_teacher(teacher_id:int,request:schemas.ShowTeacher,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return teachers.update(teacher_id,request,db)

@router.delete("/delete/{teacher_id}")
async def delete_teacher(teacher_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return teachers.delete(teacher_id,db)

@router.patch("/password/{email}")
async def update_password(email:str,request:schemas.Password,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return teachers.update_password_func(email,request,db)