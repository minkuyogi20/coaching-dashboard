from fastapi import APIRouter, Depends, status, HTTPException
from typing import List
from sqlalchemy.orm import Session

import schemas,models,database,oauth2
from routerMethods import students

router=APIRouter(
    prefix="/students",
    tags=["Students"]
)

@router.get("/get",response_model=List[schemas.Student])
async def get_all_students(db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin","Student","Teacher"]))):
    return students.get_students(db)

@router.get("/get/{student_id}",response_model=List[schemas.Student])
async  def get_teacher_with_id(student_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return students.get_student_with_id(student_id,db)

@router.post("/create",status_code=status.HTTP_201_CREATED)
async def create_student(request:schemas.CreateStudent,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return students.create(request,db)

@router.put("/update/{student_id}")
async def update_student(student_id:int,request:schemas.ShowStudent,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return students.update(student_id,request,db)

@router.delete("/delete/{student_id}")
async def delete_student(student_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return students.delete(student_id,db)

@router.patch("/password/{email}")
async def update_password(email:str,request:schemas.Password,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return students.update_password_func(email,request,db)