import oauth2
from fastapi import APIRouter,Depends,status
from typing import List
from sqlalchemy.orm import Session
import schemas,models,database
from routerMethods import batches

router=APIRouter(
    prefix="/batch",
    tags=["Batches"]
)

@router.get("/get",response_model=List[schemas.Batch])
async def get_all_batches(db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return batches.get_all(db)

@router.get("/get/{batch_id}",response_model=List[schemas.Batch])
async def get_all_batches_with_id(batch_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return batches.get_all_with_id(batch_id,db)

@router.post("/post",status_code=status.HTTP_201_CREATED)
async def create_batch(request:schemas.ShowBatch,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return batches.create(request,db)

# @router.post("/create",status_code=status.HTTP_201_CREATED)
# async def create_batch(request:schemas.Batch_new,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
#     return batches.create(request,db)


@router.put("/update/{batch_id}")
async def update_batch(batch_id:int,request:schemas.ShowBatch,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return batches.update(batch_id,request,db)

@router.delete("/delete/{batch_id}")
async def delete_batch(batch_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return batches.delete(batch_id,db)