from fastapi import APIRouter,Depends,status
from typing import List
from sqlalchemy.orm import Session
import schemas,models,database,oauth2
from routerMethods import branches

router=APIRouter(
    prefix="/branch",
    tags=["Branches"]
)

@router.get("/get",response_model=List[schemas.ShowBranch])
async def get_all_branches(db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return branches.get_all(db)

# @router.get("/{id}",response_model=schemas.ShowBranch)
# async def get_branch_with_id(id:int,db:Session=Depends(database.get_db)):
#     return  branches.get_branch(id,db)

@router.get("/get/{id}",response_model=List[schemas.Branch_course])
async def get_branch_with_id_new(id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return branches.get_branch_new(id,db)

# @router.get("/{id}/course",response_model=List[schemas.Branch_course])
# async def get_branch_with_course(id:int,db:Session=Depends(database.get_db)):
#     return  branches.get_branch_course(id,db)

@router.post("/create",status_code=status.HTTP_201_CREATED)
async def create_branch(request:schemas.Branch,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return branches.create(request,db)

@router.put("/update/{branch_id}")
async def update_branch(branch_id:int,request:schemas.Branch,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return branches.update(branch_id,request,db)

@router.delete("/delete/{branch_id}")
async def delete_branch(branch_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin"]))):
    return branches.delete(branch_id,db)