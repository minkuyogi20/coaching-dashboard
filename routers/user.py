import oauth2
from fastapi import APIRouter,Depends,status
from typing import List
from sqlalchemy.orm import Session
import schemas,models,database
from routerMethods import user

router=APIRouter(
    prefix="/user",
    tags=["Users"]
)

@router.get("/get",response_model=List[schemas.ShowUser],status_code=status.HTTP_200_OK)
async def get_all_user(db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin","Student","Teacher"]))):
    return user.get_all(db)

@router.get("/get/{id}",response_model=schemas.User)
async def get_user_with_id(id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin","Student","Teacher"]))):
    return user.get_with_id(id,db)

@router.post("/create",status_code=status.HTTP_201_CREATED)
async def create_user(request:schemas.User,db:Session=Depends(database.get_db)):
    return user.create(request,db)

@router.put("/update/{user_id}")
async def update_user(user_id:int,request:schemas.User,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin","Student","Teacher"]))):
    return user.update(user_id,request,db)

@router.patch("/update/{user_id}/patch")
async def update_user_patch(user_id:int,request:schemas.User,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin","Student","Teacher"]))):
    return user.update_patch(user_id,request,db)

@router.delete("/delete/{user_id}")
async def delete_user(user_id:int,db:Session=Depends(database.get_db),dependencies=Depends(oauth2.has_role(["Admin","Student","Teacher"]))):
    return user.delete(user_id,db)