from sqlalchemy import Column,String,Integer,ForeignKey
from database import Base
from sqlalchemy.orm import relationship

class Branch(Base):
    __tablename__ = "branches"

    id = Column(Integer,primary_key=True,index=True)
    address = Column(String(200),nullable=False)
    location = Column(String(30), nullable=False)
    course = relationship("Course",back_populates="branch")
    teacher = relationship("Teacher",back_populates="branch")
    student = relationship("Student", back_populates="branch")

class Course(Base):
    __tablename__ = "courses"

    id = Column(Integer,primary_key=True, index=True)
    course_name = Column(String(30),nullable=False)
    branch_id = Column(Integer,ForeignKey("branches.id"))
    batch = relationship("Batch",back_populates="course")
    branch = relationship("Branch",back_populates="course")
    teacher = relationship("Teacher",back_populates="course")
    student = relationship("Student", back_populates="course")

class Batch(Base):
    __tablename__ = "batches"

    id = Column(Integer, primary_key=True,index=True)
    timing = Column(String,nullable=False)
    course_id = Column(Integer,ForeignKey("courses.id"))
    course = relationship("Course",back_populates="batch")
    teacher = relationship("Teacher",back_populates="batch")
    student = relationship("Student", back_populates="batch")

class Teacher(Base):
    __tablename__ = "teachers"

    id = Column(Integer,primary_key=True,index=True)
    name = Column(String,nullable=False)
    email = Column(String,unique=True,nullable=False)
    address = Column(String,nullable=False)
    phone_number = Column(String,nullable=False)
    branch_id = Column(Integer,ForeignKey("branches.id"))
    course_id= Column(Integer,ForeignKey("courses.id"))
    batch_id = Column(Integer,ForeignKey("batches.id"))
    user_id = Column(Integer,ForeignKey("users.id"))

    branch = relationship("Branch",back_populates="teacher")
    course = relationship("Course",back_populates="teacher")
    batch = relationship("Batch",back_populates="teacher")
    user = relationship("User", back_populates="teacher")

class Student(Base):
    __tablename__ = "students"

    id = Column(Integer,primary_key=True,index=True)
    name = Column(String,nullable=False)
    email = Column(String,unique=True,nullable=False)
    address = Column(String,nullable=False)
    phone_number = Column(String,nullable=False)
    branch_id = Column(Integer,ForeignKey("branches.id"))
    course_id= Column(Integer,ForeignKey("courses.id"))
    batch_id = Column(Integer,ForeignKey("batches.id"))
    user_id = Column(Integer, ForeignKey("users.id"))

    branch = relationship("Branch",back_populates="student")
    course = relationship("Course",back_populates="student")
    batch = relationship("Batch",back_populates="student")
    user = relationship("User",back_populates="student")

class User(Base):
    __tablename__ = "users"

    id=Column(Integer,primary_key=True,index=True)
    email=Column(String)
    password=Column(String)
    role = Column(String) 

    teacher = relationship("Teacher",back_populates="user")
    student = relationship("Student",back_populates="user")
