from fastapi import Depends, HTTPException,status
from fastapi.security import OAuth2PasswordBearer
import token_jwt,schemas
from sqlalchemy.orm import Session

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

def get_current_user(token:str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    return token_jwt.verify_token(token,credentials_exception)

def has_role(allowed_roles: list):
    def check_role(payload:str = Depends(get_current_user)):
        role = payload.get("role")
        if role not in allowed_roles:
            raise HTTPException(status_code=403, detail="Permission denied")
        else:
            return payload
    return check_role