from fastapi import FastAPI
from routers import branches,courses,batches,teachers,students,user,authentication
import models
from database  import engine
from fastapi.middleware.cors import CORSMiddleware

app=FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(authentication.router)
app.include_router(branches.router)
app.include_router(courses.router)
app.include_router(batches.router)
app.include_router(teachers.router)
app.include_router(students.router)
app.include_router(user.router)

models.Base.metadata.create_all(bind=engine)