from pydantic import BaseModel,EmailStr
from typing import List, Optional

class ShowBranch(BaseModel):
    id:int
    address:str
    location:str

class Branch(BaseModel):
    address:str
    location:str

class ShowCourse(BaseModel):
    # id:int
    course_name:str
    branch_id:int

class Branch_course(BaseModel):
    id:int
    course_name:str
    # courses:List[dict]
    # course_list:List[dict]
    # batches:List[dict]
    # class config:
    #     orm_mode=True
    # course_id:int

class ShowCourse(BaseModel):
    # id:int
    course_name:str
    branch_id:int

class Course(BaseModel):
    id:int
    timing:str
    # branch_location:str
    # branch_address:str
    # branch_timing:List[str]

class Course_new(BaseModel):
    course_name:str
    branch_location: str
    branch_address: str

class ShowBatch(BaseModel):
    timing:str
    course_id:int

class Batch(BaseModel):
    id:int
    timing:str
    course_name:str

class Batch_new(BaseModel):
    timing:str
    course_name:str

class ShowTeacher(BaseModel):
    name:str
    email:EmailStr
    address:str
    phone_number:str
    branch_id:int
    course_id:int
    batch_id:int
    user_id:int

class Teacher(BaseModel):
    id:int
    name:str
    email:str
    address:str
    phone_number:str
    branch_address:str
    branch_location:str
    course_name:str
    batch_timings:str

class Teacher_new(BaseModel):
    name:str
    email:str
    address:str
    phone_number:str
    branch_address:str
    branch_location:str
    course_name:str
    batch_timings:str

class ShowStudent(BaseModel):
    name:str
    email:EmailStr
    address:str
    phone_number:str
    branch_id:int
    course_id:int
    batch_id:int
    user_id:int

class CreateStudent(BaseModel):
    name: str
    email: EmailStr
    address: str
    phone_number: str
    branch_id: int
    course_id: int
    batch_id: int
    user_id:int|None
    role:str

class Student(BaseModel):
    id:int
    name:str
    email:EmailStr
    address:str
    phone_number:str
    branch_address:str
    branch_location:str
    course_name:str
    batch_timings:str

class User(BaseModel):
    email:str
    password:str
    role:str

class User_Patch(BaseModel):
    email:str|None = None
    password:str|None = None
    role:str|None = None

class ShowUser(User):
    id:int

class Login(BaseModel):
    username:str
    password:str
    class config():
        orm_mode=True

class Token(BaseModel):
    access_token: str
    # refresh_token: str
    token_type: str

class TokenData(BaseModel):
    email : str | None = None

class Refresh(BaseModel):
    refresh_token:str

class EmailSchema(BaseModel):
    email: List[EmailStr]

class Password(BaseModel):
    password: str
    confirm_password:str